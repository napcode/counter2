import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
import './counter.dart';

class Storage {
  static Future<bool> save(List<Counter> counters) async {
    var data = counters.map((c) => c.toJson()).toList();
    final SharedPreferences storage = await SharedPreferences.getInstance();
    bool result = await storage.setString('counters', jsonEncode(data));

    return result;
  }

  static Future<List<Counter>> restore() async {
    List<Counter> counters = [];
    final SharedPreferences storage = await SharedPreferences.getInstance();
    final String? countersStr = storage.getString('counters');
    if (countersStr != null) {
      for (var c in jsonDecode(countersStr)) {
        var counter = c as Map<String, dynamic>;
        counters.add(Counter.fromJson(counter));
      }
    }

    return counters;
  }
}
