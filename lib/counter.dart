import 'package:flutter/material.dart';
import 'dart:async';
import './event_bus.dart';
import './events.dart';

class Counter {
  Counter({required this.name, this.value = 0});
  String name;
  int value;

  Counter.fromJson(Map<String, dynamic> json)
      : name = json['name'],
        value = json['value'];

  Map<String, dynamic> toJson() => {
        'name': name,
        'value': value,
      };
}

class CounterRow extends StatefulWidget {
  const CounterRow(
      {super.key,
      required this.id,
      required this.counter,
      required this.editable});
  final int id;
  final bool editable;
  final Counter counter;

  @override
  State<CounterRow> createState() => _CounterRowState();
}

class _CounterRowState extends State<CounterRow> {
  _CounterRowState();

  Timer? _name_debounce, _value_debounce;

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Flexible(
          fit: FlexFit.tight,
          child: widget.editable
              ? Container()
              : GestureDetector(
                  onTap: () {
                    EventBus.send(DecrementButtonClicked(id: widget.id));
                  },
                  onLongPress: () {
                    _dialogBuilder(context, widget.id, "-");
                  },
                  child: const Icon(
                    Icons.remove_circle,
                    size: 60,
                    color: Colors.blue,
                  ),
                ),
        ),
        Flexible(
          flex: 2,
          fit: FlexFit.tight,
          child: Center(
            child: widget.editable
                ? TextField(
                    decoration: InputDecoration(
                      border: const OutlineInputBorder(),
                      hintText: widget.counter.name.toString(),
                    ),
                    onChanged: (text) {
                      if (_name_debounce?.isActive ?? false) {
                        _name_debounce?.cancel();
                      }
                      _name_debounce =
                          Timer(const Duration(milliseconds: 500), () {
                        EventBus.send(
                            CounterNameChanged(id: widget.id, name: text));
                      });
                    },
                  )
                : Text(
                    widget.counter.name.toString(),
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.headline5,
                  ),
          ),
        ),
        Flexible(
          fit: FlexFit.tight,
          child: Center(
            child: widget.editable
                ? TextField(
                    decoration: InputDecoration(
                      border: const OutlineInputBorder(),
                      hintText: widget.counter.value.toString(),
                    ),
                    onChanged: (text) {
                      if (_value_debounce?.isActive ?? false) {
                        _value_debounce?.cancel();
                      }
                      _value_debounce =
                          Timer(const Duration(milliseconds: 500), () {
                        EventBus.send(CounterValueChanged(
                            id: widget.id, value: int.parse(text)));
                      });
                    },
                  )
                : Text(
                    widget.counter.value.toString(),
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.headline4,
                  ),
          ),
        ),
        Flexible(
          fit: FlexFit.tight,
          child: widget.editable
              ? IconButton(
                  onPressed: () {
                    EventBus.send(DeleteButtonClicked(id: widget.id));
                  },
                  icon: const Icon(
                    Icons.delete,
                    size: 60,
                    color: Colors.red,
                  ),
                )
              : GestureDetector(
                  onTap: () {
                    EventBus.send(IncrementButtonClicked(id: widget.id));
                  },
                  onLongPress: () {
                    _dialogBuilder(context, widget.id, "+");
                  },
                  child: const Icon(
                    Icons.add_circle,
                    size: 60,
                    color: Colors.blue,
                  ),
                ),
        ),
      ],
    );
  }

  @override
  void dispose() {
    _name_debounce?.cancel();
    _value_debounce?.cancel();
    super.dispose();
  }
}

Future<void> _dialogBuilder(BuildContext context, int id, String sign) {
  return showDialog<void>(
    context: context,
    builder: (BuildContext context) {
      return Dialog(
        child: SizedBox(
          width: 500,
          height: 500,
          child: Center(
            child: SingleChildScrollView(
              child: Wrap(
                children: List.generate(99, (index) {
                  return Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ElevatedButton(
                      onPressed: () {
                        if (sign == "+") {
                          EventBus.send(
                              IncrementButtonClicked(id: id, value: index + 1));
                        } else {
                          EventBus.send(
                              DecrementButtonClicked(id: id, value: index + 1));
                        }
                        Navigator.of(context).pop();
                      },
                      style: ElevatedButton.styleFrom(
                        fixedSize: const Size(60, 60),
                        shape: const CircleBorder(),
                      ),
                      child: Text(
                        '${index + 1}',
                        style: Theme.of(context).textTheme.headlineSmall,
                      ),
                    ),
                  );
                }),
              ),
            ),
          ),
        ),
      );
    },
  );
}
