import './event_bus.dart';

class AddButtonClicked extends Event {
  @override
  get eventName => "add_button_clicked";
}

class ResetButtonClicked extends Event {
  @override
  get eventName => "reset_button_clicked";
}

class EditButtonClicked extends Event {
  @override
  get eventName => "edit_button_clicked";
}

class IncrementButtonClicked extends Event {
  @override
  get eventName => "increment_button_clicked";

  final int id;
  final int value;

  IncrementButtonClicked({required this.id, this.value = 1});

  @override
  Map<String, dynamic> toJson() => {
        'eventName': eventName,
        'id': id,
        'value': value,
      };
}

class DecrementButtonClicked extends Event {
  @override
  get eventName => "decrement_button_clicked";
  final int id;
  final int value;

  DecrementButtonClicked({required this.id, this.value = 1});

  @override
  Map<String, dynamic> toJson() => {
        'eventName': eventName,
        'id': id,
        'value': value,
      };
}

class DeleteButtonClicked extends Event {
  @override
  get eventName => "delete_button_clicked";
  final int id;

  DeleteButtonClicked({required this.id});

  @override
  Map<String, dynamic> toJson() => {
        'eventName': eventName,
        'id': id,
      };
}

class CounterNameChanged extends Event {
  @override
  get eventName => "counter_name_changed";
  final int id;
  final String name;

  CounterNameChanged({required this.id, required this.name});

  @override
  Map<String, dynamic> toJson() => {
        'eventName': eventName,
        'id': id,
        'name': name,
      };
}

class CounterValueChanged extends Event {
  @override
  get eventName => "counter_value_changed";

  final int id;
  final int value;

  CounterValueChanged({required this.id, required this.value});

  @override
  Map<String, dynamic> toJson() => {
        'eventName': eventName,
        'id': id,
        'value': value,
      };
}
