import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart' show kDebugMode;
import 'dart:async' show StreamSubscription;
import './event_bus.dart';
import './events.dart';
import './counter.dart';
import './storage.dart';
import './bottom_app_bar.dart';
import './audio.dart';

void main() {
  runApp(const MyApp());
  if (kDebugMode) {
    EventBus.controller.stream.listen((event) => print(event.toJson()));
  }
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Counter',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSwatch().copyWith(
          primary: Colors.orange,
          secondary: Colors.teal,
        ),
      ),
      home: const MyHomePage(title: 'Counter'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  StreamSubscription? eventBusSubscription;
  List<Counter> counters = [];
  bool editable = false;

  _MyHomePageState() {
    eventBusSubscription = EventBus.stream.listen((event) {
      setState(() {
        switch (event.runtimeType) {
          case AddButtonClicked:
            counters.add(Counter(name: ''));
            break;
          case IncrementButtonClicked:
            Audio.play(Sound.increment);
            IncrementButtonClicked e = event;
            counters[e.id].value += e.value;
            break;
          case DecrementButtonClicked:
            Audio.play(Sound.decrement);
            DecrementButtonClicked e = event;
            counters[e.id].value -= e.value;
            break;
          case DeleteButtonClicked:
            DeleteButtonClicked e = event;
            counters.removeAt(e.id);
            break;
          case EditButtonClicked:
            editable = !editable;
            break;
          case CounterNameChanged:
            CounterNameChanged e = event;
            counters[e.id].name = e.name;
            break;
          case CounterValueChanged:
            CounterValueChanged e = event;
            counters[e.id].value = e.value;
            break;
          case ResetButtonClicked:
            for (var c in counters) {
              c.value = 0;
            }
            break;
        }
        Storage.save(counters);
      });
    });
  }

  @override
  void initState() {
    Future.delayed(Duration.zero, () async {
      var loaded_counters = await Storage.restore();
      setState(() => counters = loaded_counters);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: ListView(
          itemExtent: getItemExtent(),
          shrinkWrap: true,
          padding: const EdgeInsets.only(top: 8, bottom: 8),
          children: <Widget>[
            for (int i = 0; i < counters.length; ++i)
              CounterRow(id: i, counter: counters[i], editable: editable)
          ],
        ),
      ),
      bottomNavigationBar: const MyBottomAppBar(),
    );
  }

  double getItemExtent() {
    if (counters.length < 4) {
      return 140;
    } else if (counters.length < 6) {
      return 100;
    }

    return 80;
  }

  @override
  void dispose() {
    eventBusSubscription?.cancel();
    super.dispose();
  }
}
