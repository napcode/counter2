import 'package:audioplayers/audioplayers.dart';

enum Sound { increment, decrement }

class Audio {
  static var player = AudioPlayer();
  static var paths = {
    Sound.increment: 'increment.wav',
    Sound.decrement: 'decrement.wav',
  };

  static void play(Sound sound) async {
    await player.stop();
    await player.play(AssetSource(paths[sound]!));
  }
}
