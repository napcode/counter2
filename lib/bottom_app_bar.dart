import 'package:flutter/material.dart';
import './event_bus.dart';
import './events.dart';

class MyBottomAppBar extends StatelessWidget {
  const MyBottomAppBar({super.key});

  @override
  Widget build(BuildContext context) {
    return BottomAppBar(
      child: Padding(
        padding: const EdgeInsets.only(top: 5, bottom: 5, left: 10, right: 10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            IconButton(
              onPressed: () => EventBus.send(AddButtonClicked()),
              icon: Icon(
                Icons.add,
                size: 20,
                color: Theme.of(context).primaryColor,
              ),
            ),
            IconButton(
              onPressed: () => EventBus.send(EditButtonClicked()),
              icon: Icon(
                Icons.edit,
                size: 20,
                color: Theme.of(context).primaryColor,
              ),
            ),
            IconButton(
              onPressed: () => _dialogBuilder(context),
              icon: Icon(
                Icons.refresh,
                size: 20,
                color: Theme.of(context).primaryColor,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

Future<void> _dialogBuilder(BuildContext context) {
  return showDialog<void>(
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        title: const Text("Are You sure?"),
        actions: <Widget>[
          TextButton(
            style: TextButton.styleFrom(
              textStyle: Theme.of(context).textTheme.labelLarge,
            ),
            child: const Text('Cancel'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
          TextButton(
            style: TextButton.styleFrom(
              textStyle: Theme.of(context).textTheme.labelLarge,
            ),
            child: const Text('Reset'),
            onPressed: () {
              EventBus.send(ResetButtonClicked());
              Navigator.of(context).pop();
            },
          )
        ],
      );
    },
  );
}
